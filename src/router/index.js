/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require('./import-' + process.env.NODE_ENV)

const router = new Router({
  routes: [
    { path: '/404', component: _import('common/404'), name: '404', meta: { title: '404未找到' } },
    { path: '/login', component: _import('common/login'), name: 'login', meta: { title: '登录' } },

    { path: '/',
      component: _import('main'),
      name: 'main',
      redirect: {name: 'home'},
      meta: { title: '主入口整体布局' },
      children: [
        { path: '/home', component: _import('common/home'), name: 'home', meta: { title: '首页', isTab: false } },
        // 教育教学管理路由模块
        { path: '/subject', component: _import('education/subject'), name: 'subject', meta: { title: '科目管理', isTab: false } },
        { path: '/question', component: _import('education/question'), name: 'question', meta: { title: '试题管理', isTab: false } },
       // { path: '/card', component: _import('education/card'), name: 'card', meta: { title: '充值卡管理' } },
        { path: '/course', component: _import('education/course'), name: 'course', meta: { title: '课程管理', isTab: false } },
        // eslint-disable-next-line standard/object-curly-even-spacing
        { path: '/school', component: _import('education/school'), name: 'school', meta: { title: '学校管理'} },
        { path: '/student', component: _import('education/student'), name: 'student', meta: { title: '学员管理', isTab: false } },
        { path: '/errorQuestion', component: _import('education/errorQuestion'), name: 'student', meta: { title: '错题本', isTab: false } },
        { path: '/baiduMap', component: _import('education/baidu-map'), name: 'baidu-map', meta: { title: '百度地图' } },
        { path: '/testPaper', component: _import('education/testPaper'), name: 'testPaper', meta: { title: '试卷管理', isTab: false } },
        { path: '/AiPaperSetting', component: _import('education/AiPaperSetting'), name: 'AiPaperSetting', meta: { title: '错题组卷设置', isTab: false } },
        { path: '/studyHistory', component: _import('education/studyHistory'), name: 'studyHistory', meta: { title: '学习记录', isTab: false } },
        { path: '/languagePoints', component: _import('education/languagePoints'), name: 'languagePoints', meta: { title: '知识点管理', isTab: false } },
       // { path: '/userInfo', component: _import('education/userInfo'), name: 'userInfo', meta: { title: '用户管理' } },
        { path: '/questionHistory', component: _import('education/questionHistory'), name: 'questionHistory', meta: { title: '答题记录', isTab: false } },
        // eslint-disable-next-line standard/object-curly-even-spacing
        // eslint-disable-next-line standard/object-curly-even-spacing
        { path: '/studentQuestionHistory', component: _import('education/studentQuestionHistory'), name: 'studentQuestionHistory', meta: { title: '学员模式试题', isTab: false }},
        // eslint-disable-next-line standard/object-curly-even-spacing
        { path: '/studentPaperHistory', component: _import('education/studentPaperHistory'), name: 'studentPaperHistory', meta: { title: '学员试卷试题', isTab: false }},
        // eslint-disable-next-line standard/object-curly-even-spacing
        { path: '/correctExam', component: _import('education/correctExam'), name: 'correctExam', meta: { title: '学员试卷试题', isTab: false }},
        // 系统设置路由模块
        { path: '/admin', component: _import('system/admin'), name: 'admin', meta: { title: '管理员列表', isTab: false } },
        { path: '/role', component: _import('system/role'), name: 'role', meta: { title: '角色列表', isTab: false } },
        { path: '/imageSetting', component: _import('system/imageSetting'), name: 'imageSetting', meta: { title: '图片设置', isTab: false } },
        { path: '/menu', component: _import('system/menu'), name: 'menu', meta: { title: '菜单列表', isTab: false } },
        { path: '/log', component: _import('system/log'), name: 'log', meta: { title: '日志列表', isTab: false } },
        { path: '/message', component: _import('system/message'), name: 'message', meta: { title: '系统消息列表', isTab: false } },
        { path: '/theme', component: _import('common/theme'), name: 'theme', meta: { title: '主题' } },
     //   { path: '/weChat', component: _import('system/weChat'), name: 'weChat', meta: { title: '微信公众号配置' } },
        { path: '/statistics/exam', component: _import('statistics/exam'), name: 'exam', meta: { title: '考试统计', isTab: false } },
        { path: '/studentExam', component: _import('education/studentExam'), name: 'studentExam', meta: { title: '考试统计', isTab: false } },
        { path: '/dict', component: _import('system/dict'), name: 'dict', meta: { title: '字典管理', isTab: false } },
        { path: '/dictValue', component: _import('system/dictValue'), name: 'dictValue', meta: { title: '字典值管理', isTab: false, target_new: false } },
        { path: '/intellectPaper', component: _import('education/intellect-paper'), name: 'intellectPaper', meta: { title: '智能组卷', isTab: false } },
        { path: '/paperQuestionManager', component: _import('education/paperQuestionManager'), name: 'paperQuestionManager', meta: { title: '试卷试题管理', isTab: false } }
      ]
    }
  ]
})

// vue  路由全局拦截器
router.beforeEach(function (to, from, next) {
  let token = localStorage.getItem('token')
  if (to.path === '/login') {
    if (token) {
      location.href = '/'
    } else {
      next()
    }
  } else {
    if (!token) {
      next({ name: 'login' }) // 没有token 跳转登录页面
    }
    next()
  }
})

export default router
