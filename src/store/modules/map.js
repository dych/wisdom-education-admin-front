export default {
  namespaced: true,
  state: {
    positionInfo: {},
    schoolForm: {},
    paperQuestionParams: {},
    studentExamInfo: {},
    positionList: []
  },

  getters: {

    getPaperQuestionParams (state) {
      if (state.paperQuestionParams.id) {
        return state.paperQuestionParams
      }
      return JSON.parse(sessionStorage.getItem('paperQuestionParams'))
    },

    getPositionInfo (state) {
      if (state.positionInfo.lng) {
        return state.positionInfo
      }
      return JSON.parse(sessionStorage.getItem('positionInfo'))
    },

    getStudentExamInfo (state) {
      if (state.studentExamInfo.paper_name) {
        return state.studentExamInfo
      }
      return JSON.parse(sessionStorage.getItem('studentExamInfo'))
    }
  },

  mutations: {

    updateStudentExamInfo (state, studentExamInfo) {
      sessionStorage.setItem('studentExamInfo', JSON.stringify(studentExamInfo))
      state.studentExamInfo = studentExamInfo
    },

    updatePositionInfo (state, positionInfo) {
      sessionStorage.setItem('positionInfo', JSON.stringify(positionInfo))
      state.positionInfo = positionInfo
    },

    updatePaperQuestionParams (state, paperQuestionParams) {
      sessionStorage.setItem('paperQuestionParams', JSON.stringify(paperQuestionParams))
      state.paperQuestionParams = paperQuestionParams
    },

    updatePositionList (state, positionList) {
      state.positionList = positionList
    },

    updateSchoolForm (state, form) {
      state.schoolForm = form
    }
  }
}
